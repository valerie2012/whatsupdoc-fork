Rails.application.routes.draw do
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin', locale: "en"
  devise_for :users, :controllers => { :omniauth_callbacks => "omniauth_callbacks" }

  scope "/:locale", locale: /#{I18n.available_locales.join("|")}/ do
    resources :doctors do
      resources :reviews
    end

    resources :users
     
    resources :filters
   
    resources :articles

    resources :searches

    resources :appointments

    resources :time_slots

    resources :jakartas

    get "abouts/show"

    get "contacts/show"
    post "contacts/send_message"

    get "privacy_policies/show"
   
    
    root 'jakartas#index'

    #match 'auth/:provider/callback', to: 'sessions#create', via: [:get, :post]
    #match 'auth/failure', to: redirect('/'), via: [:get, :post]
    #match 'signout', to: 'sessions#destroy', as: 'signout', via: [:get, :post]

    #match 'auth/:provider/callback', to: 'sessions#create', via: [:get, :post]
    #match 'auth/failure', to: redirect('/'), via: [:get, :post]
    #match 'signout', to: 'sessions#destroy', as: 'signout', via: [:get, :post]
  end
  
  get "/*path", to: redirect("/#{I18n.default_locale}/%{path}", status: 302), constraints: {path: /(?!(#{I18n.available_locales.join("|")})\/).*/}, format: false
end
