# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170403085258) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "appointments", force: :cascade do |t|
    t.string   "insurance"
    t.string   "reason_for_visit"
    t.integer  "time_slot_id"
    t.integer  "user_id"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.boolean  "new_patient"
    t.datetime "appointment_datetime"
    t.integer  "doctor_id"
    t.string   "patient_name"
    t.string   "date_of_appointment"
    t.string   "status"
    t.integer  "age"
    t.string   "gender"
  end

  create_table "articles", force: :cascade do |t|
    t.string   "title"
    t.string   "description"
    t.integer  "category_id"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.string   "article_img_file_name"
    t.string   "article_img_content_type"
    t.integer  "article_img_file_size"
    t.datetime "article_img_updated_at"
    t.string   "category_name"
  end

  create_table "categories", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "doctors", force: :cascade do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "specialty"
    t.string   "email"
    t.string   "mobile_number"
    t.integer  "zipcode"
    t.string   "education"
    t.text     "hospital_affiliations"
    t.string   "language_spoken"
    t.string   "certification"
    t.string   "proffesional_membership"
    t.text     "proffesional_statement"
    t.string   "in_network_insurance"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.string   "license_number"
    t.string   "city"
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
    t.boolean  "status"
    t.integer  "user_id"
    t.integer  "specialty_id"
    t.string   "name"
    t.integer  "schedule_id"
  end

  create_table "filters", force: :cascade do |t|
    t.string   "keywords"
    t.string   "category"
    t.integer  "view"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "hospitals", force: :cascade do |t|
    t.string   "name"
    t.text     "address"
    t.string   "telphone"
    t.string   "fax"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "categ"
  end

  create_table "reviews", force: :cascade do |t|
    t.integer  "rating"
    t.text     "comment"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "user_id"
    t.integer  "doctor_id"
  end

  create_table "schedules", force: :cascade do |t|
    t.integer  "doctor_id"
    t.string   "name"
    t.datetime "valid_from_date"
    t.datetime "valid_to_date"
    t.integer  "time_slot_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  create_table "searches", force: :cascade do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "city"
    t.string   "in_network_insurance"
    t.string   "specialty"
    t.string   "hospital_affiliations"
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
  end

  create_table "specialties", force: :cascade do |t|
    t.string   "name"
    t.string   "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.integer  "doctor_id"
  end

  create_table "time_slots", force: :cascade do |t|
    t.string   "name"
    t.time     "start_time"
    t.time     "end_time"
    t.integer  "time_off"
    t.integer  "schedule_id"
    t.date     "date"
    t.datetime "start_date"
    t.datetime "end_date"
  end

  create_table "translations", force: :cascade do |t|
    t.string   "locale"
    t.string   "key"
    t.text     "value"
    t.text     "interpolations"
    t.boolean  "is_proc",        default: false
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                              default: "", null: false
    t.string   "encrypted_password",                 default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                      default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
    t.string   "first_name"
    t.string   "last_name"
    t.datetime "date_of_birth"
    t.string   "gender"
    t.string   "address"
    t.string   "city"
    t.string   "state"
    t.integer  "zipcode"
    t.string   "role"
    t.integer  "mobile_number"
    t.string   "provider",               limit: 255
    t.string   "uid"
    t.string   "name",                   limit: 255
    t.string   "oauth_token",            limit: 255
    t.datetime "oauth_expires_at"
  end

end
