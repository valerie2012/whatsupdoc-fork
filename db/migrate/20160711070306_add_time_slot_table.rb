class AddTimeSlotTable < ActiveRecord::Migration
  def change
  	create_table :time_slots do |t|
  		t.string :name 
  		t.time :start_time
  		t.time :end_time
  		t.integer :time_off
  	end
  end
end
