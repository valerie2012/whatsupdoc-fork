class AddAgeGenderToAppointments < ActiveRecord::Migration
  def change
  	add_column :appointments, :age, :integer
  	add_column :appointments, :gender, :string
  end
end
