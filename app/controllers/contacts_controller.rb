class ContactsController < ApplicationController

	def show
	end

	def send_message
		data = params[:email]
		if data[:name].nil? || data[:email].nil? || data[:message].nil?
			redirect_to :back
			flash[:notice]='Please fill all the fields'
		else
			@name = data[:name]
			@email = data[:email]
			@message = data[:message]
			ContactMailer.send_message(@name, @email, @message).deliver_now
			redirect_to contacts_show_path
		end
	end

end