class ArticlesController < ApplicationController

	before_action :find_article, only: [:show, :edit, :update, :destroy]

	def index
		# @article = Article.all.order('created_at DESC')
		# @article = Article.where("category_id = '3'")
		# @cat1 = Article.where(category_id: 1).last(1)
		# @cat2 = Article.where(category_id: 2).last(1)
		# @cat3 = Article.where(category_id: 3).last(1)

		# @article = Article.all.order('created_at DESC').paginate(page: params[:page], per_page: 2)
		# @articleTotal = Article.count
		# @articlePage = @articleTotal % 2
		# @categories = Category.all.map{ |c| [c.name, c.id]}

		# if params[:category].blank?
		# 	@article = Article.all.order('created_at DESC').paginate(page: params[:page], per_page: 2)
		# else
		#  	@category_id = Category.find_by(name: params[:category]).id
		#  	@article = Article.where(:category_id => @category_id).order("created_at DESC").paginate(page: params[:page], per_page: 2)
		# end
		
		# @articleTotal = Article.count
		# @articlePage = @articleTotal % 2
		# @category = Category.all
		#@categories = Category.all.map{ |c| [c.name, c.id]}

		# if params[:search].blank?
		# 	@article = Article.all.order('created_at DESC').paginate(page: params[:page], per_page: 2)
		# else
		# 	@article = Article.where(["title LIKE ?", "%#{params[:search]}%"]).paginate(page: params[:page], per_page: 2)	
		# end

		@article  = Article.search_articles(params[:filter]).paginate(page: params[:page], per_page: 3)

		@article1 = Article.where(:category_name => 'diet').paginate(page: params[:page], per_page: 1)
		@article2 = Article.where(:category_name => 'healthy').paginate(page: params[:page], per_page: 1)
		@article3 = Article.where(:category_name => 'news').paginate(page: params[:page], per_page: 1)
		
		@category = Article.uniq.pluck(:category_name)
		@date     = Article.all
		@doctor   = Doctor.all

	end

	def show
	end

	def new
		@article = Article.new
		@category = Category.new

	end

	def create
		@article = Article.new(article_params)
	end

	def update

	end

	def edit
	end

	def destroy
	end

	private
		def article_params
			params.require(:article).permit(:category_id)
		end

		def find_article
			@article = Article.find(params[:id])
		end
end
