class SearchesController < ApplicationController

def new
	@search = Search.new
	@in_network_insurance = Doctor.uniq.pluck(:in_network_insurance)
	@city = Doctor.uniq.pluck(:city)
	@specialty = Doctor.uniq.pluck(:specialty)

end

def create
	@search = Search.create(search_params)
	redirect_to @search
end

def show
	@s = Search.find(params[:id])

	@search = Search.new
	@in_network_insurance = Doctor.uniq.pluck(:in_network_insurance)
	@city = Doctor.uniq.pluck(:city)
	@specialty = Doctor.uniq.pluck(:specialty)

end

private

def search_params
	params.require(:search).permit(:specialty, :in_network_insurance, :city, :first_name, :last_name)
end

end
