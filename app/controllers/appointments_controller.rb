class AppointmentsController < ApplicationController
	
	before_action :find_appointment, only: [:show]

	def index
		user = User.all
	end

	def new
		@appointment = Appointment.new
	end

	def create
		@appointment = Appointment.new(appointment_params)

		if @appointment.save
			redirect_to :back
			AppointmentMailer.welcome_email(current_user).deliver
			flash[:notice]='Thank You for Booking!'
		else
			redirect_to :back
			flash[:notice]='Time has already taken.'
		end	
	end

	private

	def appointment_params
		params.require(:appointment).permit(:insurance, :reason_for_visit, :new_patient, :profession_id, :time_slot_id, :user_id, :patient_name, :date_of_appointment, :doctor_id)
	end

	def find_appointment
		@appointment = Appointment.find(params[:id])
	end

end
