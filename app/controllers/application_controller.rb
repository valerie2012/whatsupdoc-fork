class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

   before_filter :configure_permitted_parameters, if: :devise_controller?
  before_filter :set_locale
  before_filter :get_language
  # 
  def after_sign_in_path_for(resource)

	  if resource.role == "doctor"
	    doctors_path 
	  elsif resource.role == "admin"
	    rails_admin.dashboard_path 
	  elsif resource.role == "guest"
	    root_path 
	  end
	  
  end

  def configure_permitted_parameters
  	devise_parameter_sanitizer.for(:account_update){|u| u.permit(:first_name, :last_name, :date_of_birth, :gender, :mobile_number, :city, :state, :zipcode, :email)}
  	
  	devise_parameter_sanitizer.for(:sign_up){|u| u.permit(:first_name, :last_name, :date_of_birth, :gender, :mobile_number, :city, :state, :zipcode, :email, :password, :password_confirmation, :address, :role)}
  end

  def set_locale

    m_locales_system = I18n.available_locales.map { |l| l.to_s.prepend '/' }

    #if there is single param
    s_locales_param = I18n.available_locales.map { |l| l.to_s.prepend "\?new_locale\=" }

    #if there are multiple param 
    m_locales_param = I18n.available_locales.map { |l| l.to_s.prepend "\&new_locale\=" }

    if params[:new_locale].present?
      @new_locale = params[:new_locale]
      
      #/ru => /en
      @removed_old_locale = request.fullpath.gsub(/^#{m_locales_system.map{|s| "(#{s})"}.join("|")}/,"\/#{@new_locale}")
      
      #/ru/ => /en/
      @removed_old_locale = @removed_old_locale.gsub(/^#{m_locales_system.map{|s| "(#{s})"}.join("|")}\//,"\/#{@new_locale}\/")
      
      #remove ?new_locale=en
      @new_link = "#{@removed_old_locale}".gsub(Regexp.union(s_locales_param), '')

      #remove &new_locale=en
      @new_link = "#{@new_link}".gsub(Regexp.union(s_locales_param), '')
      
      @root_url = { "en" => "http://whatsupdoc.asia", "id" => "http://apakabardoc.com"}
      redirect_to "#{@root_url[@new_locale]}#{@new_link}"
    else
      locale = params[:locale].to_s.strip.to_sym
      I18n.locale = I18n.available_locales.include?(locale) ? locale : I18n.default_locale
    end
  end

  def default_url_options
    { locale: I18n.locale }
  end

  def get_language
    # Set the option selected value
    @language_options = { "US" => "en", "Bahamas Indonesia" => "id" }

    if params.has_key?(:locale)  
      @language = params[:locale]
    else 
      @language = 'en'
    end
  end

  #Conflict with rails_admin

  #helper method to access whoever logged in at the moment
  #private

  #def current_user
   # @current_user ||= User.find(session[:user_id]) if (session[:user_id]) 
  #end

  #helper_method :current_user

end
