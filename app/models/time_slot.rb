class TimeSlot < ActiveRecord::Base
belongs_to :schedule

	RailsAdmin.config do |config|
		config.model 'TimeSlot' do
			navigation_icon 'fa fa-clock-o'

			create do 
			    field :schedule					
				field :start_date
				field :end_date
				field :time_off, :enum do 
					enum do 
						[10,15,20,25,30,35,40,45,50,55,60]
					end
				end
							
			end

			list do		
				field :schedule		
				field :start_date
				field :end_date
				field :time_off			
			end

			edit do 	
				field :schedule			
				field :start_date
				field :end_date
				field :time_off, :enum do 
					enum do 
						[10,15,20,25,30,35,40,45,50,55,60]
					end
				end
				
			end

		end
	end
end
