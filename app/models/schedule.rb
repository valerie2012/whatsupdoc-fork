class Schedule < ActiveRecord::Base
	belongs_to :doctor
	has_many :time_slot

	RailsAdmin.config do |config|
		config.model 'Schedule' do
			create do 
				field :doctor do
					required true
				end
				field :name
			end

			edit do
				field :name
				field :doctor
			end
			
			list do
				field :name
				field :doctor
			end
		end
	end

end
