class ContactMailer < ApplicationMailer
	default from: "from@example.com"
  layout 'mailer'
  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.contact_mailer.message.subject
  #
  def send_message(name, email, message)
  	@sender = name
  	@email = email
    @message = message

    #to be receive by admin
    mail to: "claire.bayoda@jumpdigital.asia", subject: "WhatsUpDoc Contact Us email"
  end
end
